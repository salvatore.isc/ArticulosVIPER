//
//  ArticleProtocols.swift
//  ArticulosVIPER
//
//  Created by Salvador Lopez on 29/06/23.
//

import Foundation

// Este protocolo sera implementado en ArticlePresenter.swift
protocol ViewToPresenterProtocol: AnyObject {
    var view: PresenterToViewProtocol? {get set}
    var interactor: PresenterToInteractorProtocol? {get set}
    var router: PresenterToRouterProtocol? {get set}
    func startFetchingArticle()
}

// Este protocolo sera implementado en ViewController.swift
protocol PresenterToViewProtocol: AnyObject{
    func showArticle(articles:[Articulo])
    func showError()
}

// Este protocolo sera implenmentado en el ArticleRouter.swift
protocol PresenterToRouterProtocol: AnyObject{
    func createModule() -> ViewController
    func pushToAnotherModule()
}

// Este protocolo sera implementado en el ArticleInteractor.swift
protocol  PresenterToInteractorProtocol: AnyObject{
    var presenter: InteractorToPresenterProtocol? {get set}
    func fetchArticle()
}

// Este protocolo sera implementado en el ArticlePresenter.swift
protocol InteractorToPresenterProtocol: AnyObject {
    func articleFetchSuccess(articles:[Articulo])
    func articleFetchFail()
}
