//
//  ArticleInteractor.swift
//  ArticulosVIPER
//
//  Created by Salvador Lopez on 29/06/23.
//

import Foundation

class ArticleInterctor: PresenterToInteractorProtocol {
    
    private let url = URL(string: Constantes.restApiUrl)
    private var articleList: ArticuloLista!
    
    var presenter: InteractorToPresenterProtocol?
    
    //Consumo de la class WS
    func fetchArticle() {
        WebService().getArticles(url: url!) { articulos in
            if let articulos = articulos {
                self.presenter?.articleFetchSuccess(articles: articulos)
            }else{
                self.presenter?.articleFetchFail()
            }
        }
    }
    
    
}
