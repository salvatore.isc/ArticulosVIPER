//
//  ViewController.swift
//  ArticulosVIPER
//
//  Created by Salvador Lopez on 29/06/23.
//

import UIKit

class ViewController: UIViewController {

    var presenter: ViewToPresenterProtocol?
    var articles = [Articulo]()
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.tableView.rowHeight = 130
        self.tableView.dataSource = self
        presenter?.startFetchingArticle()
    }
    
}

extension ViewController: PresenterToViewProtocol {
    
    func showArticle(articles: [Articulo]) {
        dump(articles)
        self.articles = articles
        tableView.reloadData()
    }
    
    func showError() {
        print("Error al recuperar articulos")
    }
    
}

extension ViewController: UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.articles.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = articles[indexPath.row].title
        cell.detailTextLabel?.text = articles[indexPath.row].description
        return cell
    }
}
