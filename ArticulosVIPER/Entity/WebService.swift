//
//  WebService.swift
//  ArticulosVIPER
//
//  Created by Salvador Lopez on 29/06/23.
//

import Foundation


class WebService{
    
    func getArticles(url:URL, completion:@escaping ([Articulo]?) -> Void ){
        URLSession.shared.dataTask(with: url){
            data, response, error in
            if let error = error{
                print("Error: \(error)")
            }else if let data = data {
                do {
                    let articleList =  try? JSONDecoder().decode(ArticuloLista.self, from: data)
                    if let articleList = articleList {
                        completion(articleList.articles)
                    }
                }catch{
                    print("Error: \(error)")
                }
            }
        }.resume()
    }
    
}
