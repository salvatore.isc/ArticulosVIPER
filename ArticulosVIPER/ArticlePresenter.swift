//
//  ArticlePresenter.swift
//  ArticulosVIPER
//
//  Created by Salvador Lopez on 29/06/23.
//

import Foundation

class ArticlePresenter: ViewToPresenterProtocol {
    
    var view: PresenterToViewProtocol?
    
    var interactor: PresenterToInteractorProtocol?
    
    var router: PresenterToRouterProtocol?
    
    func startFetchingArticle() {
        interactor?.fetchArticle()
    }
    
}

extension ArticlePresenter: InteractorToPresenterProtocol {
    
    func articleFetchSuccess(articles: [Articulo]) {
        DispatchQueue.main.async {
            self.view?.showArticle(articles: articles)
        }
    }
    
    func articleFetchFail() {
        self.view?.showError()
    }
    
}
