//
//  ArticleRouter.swift
//  ArticulosVIPER
//
//  Created by Salvador Lopez on 29/06/23.
//

import Foundation
import UIKit

class ArticleRouter: PresenterToRouterProtocol {
    
    let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
    
    func createModule() -> ViewController {
        //Definir y crear comunicacion
        let view = storyboard.instantiateViewController(withIdentifier: "mainVC") as! ViewController
        let presenter: ViewToPresenterProtocol & InteractorToPresenterProtocol = ArticlePresenter()
        let interactor: PresenterToInteractorProtocol = ArticleInterctor()
        let router: PresenterToRouterProtocol = ArticleRouter()
        
        //Comunicacion
        view.presenter = presenter
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return view
    }
    
    func pushToAnotherModule() {
        //
    }
    
}
