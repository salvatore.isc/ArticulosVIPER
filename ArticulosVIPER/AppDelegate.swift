//
//  AppDelegate.swift
//  ArticulosVIPER
//
//  Created by Salvador Lopez on 29/06/23.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        let article = ArticleRouter().createModule()
        let nc = UINavigationController()
        nc.viewControllers = [article]
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = nc
        window?.makeKeyAndVisible()
        
        return true

    }

}

